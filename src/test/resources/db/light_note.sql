-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `note_group`  (
                          `group_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '组id',
                          `user_id` int(11) NOT NULL COMMENT '用户id',
                          `group_name` varchar(255) NOT NULL COMMENT '组名',
                          PRIMARY KEY (`group_id`)
);

-- ----------------------------
-- Table structure for note
-- ----------------------------
DROP TABLE IF EXISTS `note`;
CREATE TABLE `note`  (
                         `note_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'noteId',
                         `user_id` int(11) NOT NULL COMMENT '用户id',
                         `group_id` int(11) NOT NULL COMMENT '所属群组id',
                         `title` varchar(255) NOT NULL COMMENT '标题',
                         `content` varchar(255)  NULL DEFAULT NULL COMMENT '内容',
                         `notice_content` varchar(255)  NULL DEFAULT NULL COMMENT '提醒内容',
                         `schedule_time` datetime(0) NULL DEFAULT NULL COMMENT '规划完成时间',
                         `finish_time` datetime(0) NULL DEFAULT NULL COMMENT '实际完成时间',
                         `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                         `status` int(11) NOT NULL COMMENT 'note状态，0-未完成，1-完成，2-已超时未完成，3-已超时但完成了',
                         `is_mark_down` int(11) NULL DEFAULT 0 COMMENT '0-不是，1-是',
                         PRIMARY KEY (`note_id`)
);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
                         `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
                         `user_name` varchar(255)  NOT NULL COMMENT '用户名',
                         `password` varchar(255)  NOT NULL COMMENT '密码',
                         `profile_pic` varchar(255)  NULL DEFAULT NULL COMMENT '头像url',
                         `account` varchar(255)  NOT NULL COMMENT '登陆账号（邮箱）',
                         PRIMARY KEY (`user_id`)
)
