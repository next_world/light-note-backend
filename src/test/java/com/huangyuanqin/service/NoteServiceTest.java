package com.huangyuanqin.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanqin.controller.vo.NoteVo;
import com.huangyuanqin.dao.NoteMapper;
import com.huangyuanqin.po.Note;
import com.huangyuanqin.service.impl.NoteServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author hyq
 * @date : 2020/5/1  22:28
 */
@SpringBootTest
@Transactional
public class NoteServiceTest {

    @Resource
    private NoteServiceImpl noteService;

    @Resource
    private NoteMapper noteMapper;


    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNoteTest(){
        NoteVo noteVo = noteService.getNote(1,1,1,1,1);
        System.out.println(noteVo);
        Assertions.assertNotNull(noteVo);
        Assertions.assertEquals(1L,noteVo.getPage());
        Assertions.assertEquals("test",noteVo.getNoteList().get(0).getContent());
    }

    @Test
    public void updateNoteTest(){
        Note note = new Note();
        note.setNoteId(1);
        note.setContent("test--test--test--test");
        note.setTitle("Test of test");
        System.out.println(noteMapper.selectById(1));
        noteService.updateNote(note);
        System.out.println(noteMapper.selectById(1));
    }

}
