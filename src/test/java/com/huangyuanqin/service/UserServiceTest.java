package com.huangyuanqin.service;

import com.huangyuanqin.common.CommonResult;
import com.huangyuanqin.controller.UserController;
import com.huangyuanqin.po.User;
import com.huangyuanqin.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hyq
 * @Date 2020/5/28 - 12:42
 */
@SpringBootTest
@Transactional
public class UserServiceTest {

    @Resource
    private UserServiceImpl userService;

    @Resource
    private UserController userController;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void registerTest(){
        User user = new User();
        user.setUserName("HHH");
        user.setPassword("1234");
        user.setAccount("1563809939@qq.com");
        userService.register(user);
        List<User> users = userService.list();
        for (User user1 : users) {
            System.out.println(user1);
        }
    }

    @Test
    public void getUserById(){
        CommonResult<User> userInfo = userController.getUserInfo(1);
        System.out.println(userInfo);
    }
}
