package com.huangyuanqin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LightNoteBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(LightNoteBackendApplication.class, args);
    }

}
