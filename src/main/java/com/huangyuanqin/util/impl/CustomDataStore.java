package com.huangyuanqin.util.impl;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.huangyuanqin.util.IDataStore;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author hyq
 * @date : 2021/1/6  21:33
 */
@Component
public class CustomDataStore implements IDataStore {

    /**
     * 用redis会更好的，懒得搭redis了，凑合用吧
     */
    private static final Cache<String, String> cache = CacheBuilder
            .newBuilder()
            .expireAfterWrite(2, TimeUnit.DAYS)
            .build();

    @Override
    public boolean put(String key, String value, long expire) {
        cache.put(key, value);
        return true;
    }

    @Override
    public boolean remove(String key) {
        cache.invalidate(key);
        return true;
    }

    @Override
    public boolean refresh(String key, long expire) {
        String s = cache.getIfPresent(key);
        if (s == null) {
            return false;
        }
        cache.put(key, s);
        return true;
    }

    @Override
    public String get(String key) {
        return cache.getIfPresent(key);
    }
}
