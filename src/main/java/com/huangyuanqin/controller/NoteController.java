package com.huangyuanqin.controller;

import com.huangyuanqin.common.CommonResult;
import com.huangyuanqin.controller.vo.NoteVo;
import com.huangyuanqin.po.NoteGroup;
import com.huangyuanqin.po.Note;
import com.huangyuanqin.service.NoteGroupService;
import com.huangyuanqin.service.NoteService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hyq
 * @date : 2020/4/30  16:58
 */
@RestController
@RequestMapping("/note")
public class NoteController {

    @Resource
    private NoteService noteService;

    @Resource
    private NoteGroupService noteGroupService;

    @GetMapping("/getNoteList")
    public CommonResult<NoteVo> getNoteList(@RequestParam(name = "page", defaultValue ="1" )Integer page,
                                                @RequestParam(name = "limit", defaultValue = "10") Integer limit,
                                                @RequestParam("userId")Integer userId,
                                                @RequestParam(value = "status",required = false) Integer status,
                                                @RequestParam(value = "groupId", required = false) Integer groupId){

        NoteVo noteVo = noteService.getNote(page,limit,userId,status,groupId);
        return new CommonResult<NoteVo>().operateSuccess(noteVo);
    }

    @GetMapping("/deleteNote")
    public CommonResult<Boolean> deleteNote(@RequestParam("noteId") Integer noteId){
        return new CommonResult<>().autoResult(noteService.deleteNote(noteId));
    }

    @PostMapping("/addNote")
    public CommonResult<Boolean> addNote(@RequestBody  Note note){
        return new CommonResult<>().autoResult(noteService.addNote(note));
    }

    @PostMapping("/updateNote")
    public CommonResult<Boolean> updateNote(@RequestBody  Note note){
        boolean b = noteService.updateNote(note);
        if (b){
            return new CommonResult<Boolean>().setSuccess(true).setMessage("更新成功");
        }
        else {
            return new CommonResult<Boolean>().setSuccess(false).setMessage("更新失败");
        }
    }

    @GetMapping("/getGroupList")
    public CommonResult<List<NoteGroup>> getGroup(@RequestParam("userId") Integer userId){
        return new CommonResult<List<NoteGroup>>().operateSuccess(noteGroupService.getGroupList(userId));
    }

    @PostMapping("/addGroup")
    public CommonResult<Boolean> addGroup(@RequestBody NoteGroup group){
        return new CommonResult<>().autoResult(noteGroupService.addGroup(group));
    }

    @GetMapping("/deleteGroup")
    public CommonResult<Boolean> deleteGroup(@RequestParam("groupId") Integer groupId){
        return new CommonResult<>().autoResult(noteGroupService.deleteGroup(groupId));
    }
}
