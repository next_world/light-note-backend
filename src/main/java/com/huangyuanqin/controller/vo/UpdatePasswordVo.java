package com.huangyuanqin.controller.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author hyq
 * @date : 2020/5/26  11:19
 */
@Data
@Accessors(chain = true)
public class UpdatePasswordVo {
    public Integer userId;

    public String oldPassword;

    public String newPassWord;
}
