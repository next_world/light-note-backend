package com.huangyuanqin.controller.vo;

import com.huangyuanqin.po.Note;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author hyq
 * @date : 2020/4/30  17:08
 */
@Data
@Accessors(chain = true)
public class NoteVo {
    private Long total;
    private Long page;
    private List<Note> noteList;
}
