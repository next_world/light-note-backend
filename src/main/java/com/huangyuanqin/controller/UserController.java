package com.huangyuanqin.controller;

import com.huangyuanqin.common.CommonConstant;
import com.huangyuanqin.common.CommonResult;
import com.huangyuanqin.controller.vo.UpdatePasswordVo;
import com.huangyuanqin.po.User;
import com.huangyuanqin.service.UserService;
import com.huangyuanqin.service.bo.UserBo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * @author hyq
 * @date : 2020/4/30  16:58
 */
@RestController
@RequestMapping("/user")
//@CrossOrigin
public class UserController {

    @RequestMapping("/test")
    public CommonResult<String> test(){
        return new CommonResult<String>().operateSuccess("test");
    }

    @RequestMapping("/test2")
    public CommonResult<String> test2(Integer x){
        return new CommonResult<String>().operateFail("testFail");
    }

    @Resource
    private UserService userService;

    @PostMapping("/login")
    public CommonResult<UserBo> login(@RequestBody User user) {
        UserBo userBo = userService.login(user);
        return userBo == null ? new CommonResult<UserBo>().operateFail() : new CommonResult<UserBo>().operateSuccess(userBo);
    }

    @PostMapping("/register")
    public CommonResult<Boolean> register(@RequestBody User user){
        boolean successOrNot = userService.register(user);
        if(successOrNot){
            return new CommonResult<Boolean>().setCode(CommonConstant.SUCCESS_CODE)
                    .setSuccess(true).setMessage("注册成功");
        }else{
            return new CommonResult<Boolean>().setCode(CommonConstant.FAIL_CODE)
                    .setSuccess(false).setMessage("注册失败");
        }
    }

    @PostMapping("/updateUserInfo")
    public CommonResult<User> updateUserInfo(@RequestBody User user){
        User user1 = userService.updateUserInfo(user);
        if (user1!=null){
            return new CommonResult<User>().setSuccess(true).setMessage("更新成功").setData(user1);
        }
        else{
            return new CommonResult<User>().setSuccess(false).setMessage("更新失败");
        }
    }

    @PostMapping("/updatePassword")
    public CommonResult<Boolean> updatePassword(@RequestBody UpdatePasswordVo updatePasswordVo){
        boolean b = userService.updateUserPassword(updatePasswordVo);
        if (b){
            return new CommonResult<Boolean>().setSuccess(true).setMessage("修改密码成功");
        }
        else{
            return new CommonResult<Boolean>().setSuccess(false).setMessage("修改密码失败");
        }
    }

    @GetMapping("/getUserInfo")
    public CommonResult<User> getUserInfo(@RequestParam("userId") Integer userId){
        User user = userService.getById(userId);
        if(user!=null){
            return new CommonResult<User>().operateSuccess(user);
        }else {
            return new CommonResult<User>().setMessage("用户不存在").setSuccess(false)
                    .setCode(CommonConstant.FAIL_CODE);
        }
    }

    //上传图片的，先不写
    @PostMapping("/upLoadPic")
    public CommonResult<String> upLoadPic(String pic){
        return null;
    }
}
