package com.huangyuanqin.aop;

import com.huangyuanqin.common.CommonResult;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLSyntaxErrorException;

/**
 * @author hyq
 * @date : 2020/5/1  21:09
 */
@ControllerAdvice
@Component
public class GlobalExceptionHandlerAdvice {
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public CommonResult<Void> handlerIllegalArgumentException(IllegalArgumentException ex) {
        return new CommonResult<Void>().operateFail().setMessage(ex.getMessage());
    }


    @ExceptionHandler(IllegalStateException.class)
    @ResponseBody
    public CommonResult<Void> handlerIllegalStateException(IllegalStateException ex) {
        return new CommonResult<Void>().operateFail().setMessage(ex.getMessage());
    }

    @ExceptionHandler(SQLSyntaxErrorException.class)
    @ResponseBody
    public CommonResult<Void> handlerSQLSyntaxErrorException(SQLSyntaxErrorException ex) {
        return new CommonResult<Void>().operateFail().setMessage(ex.getMessage());
    }

}
