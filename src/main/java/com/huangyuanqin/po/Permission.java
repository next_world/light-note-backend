package com.huangyuanqin.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@Data
@TableName(value = "permission")
public class Permission {
    public static final String COL_ID = "id";
    public static final String COL_NAME = "permission_name";
    public static final String COL_DESCRIPTION = "description";
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField(value = "permission_name")
    private String name;
    @TableField(value = "description")
    private String description;
}