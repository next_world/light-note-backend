package com.huangyuanqin.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@Data
@TableName(value = "role_permission")
public class RolePermission {
    public static final String COL_ID = "id";
    public static final String COL_PERMISSION_ID = "permission_id";
    public static final String COL_ROLE_ID = "role_id";
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField(value = "permission_id")
    private Integer permissionId;
    @TableField(value = "role_id")
    private Integer roleId;
}