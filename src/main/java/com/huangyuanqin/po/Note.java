package com.huangyuanqin.po;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * note
 * @author 
 */
@Data
@Accessors(chain = true)
@TableName(value = "note")
public class Note implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * noteId
     */
    @TableId(type = IdType.AUTO,value = "note_id")
    private Integer noteId;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 所属群组id
     */
    @TableField(value = "group_id")
    private Integer groupId;

    /**
     * 标题
     */
    @TableField(value = "title")
    private String title;

    /**
     * 内容
     */
    @TableField(value = "content")
    private String content;

    /**
     * 提醒内容
     */
    @TableField(value = "notice_content")
    private String noticeContent;

    /**
     * 规划完成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "schedule_time")
    private Date scheduleTime;

    /**
     * 实际完成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "finish_time")
    private Date finishTime;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * note状态，0-未完成，1-完成，2-已超时未完成，3-已超时但完成了
     */
    @TableField(value = "status")
    private Integer status;

    /**
     * 内容是否是md格式的，默认0
     * 0：不是，1：是
     */
    private Integer isMarkDown;

}