package com.huangyuanqin.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@Data
@TableName(value = "`role`")
public class Role {
    public static final String COL_ID = "id";
    public static final String COL_NAME = "role_name";
    public static final String COL_DESCRIPTION = "description";

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 角色名称
     */
    @TableField(value = "role_name")
    private String name;
    /**
     * 角色描述
     */
    @TableField(value = "description")
    private String description;
}