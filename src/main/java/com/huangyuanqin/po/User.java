package com.huangyuanqin.po;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * user
 * @author 
 */
@Data
@Accessors(chain = true)
@TableName(value = "user")
public class User implements Serializable {
    /**
     * 用户id
     */
    @TableId(type = IdType.AUTO,value = "user_id")
    private Integer userId;

    /**
     * 用户名
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * 密码
     */
    @TableField(value = "password")
    private String password;

    /**
     * 头像url
     */
    @TableField(value = "profile_pic")
    private String profilePic;

    /**
     * 登陆账号（邮箱）
     */
    @TableField(value = "account")
    private String account;

    private static final long serialVersionUID = 1L;
}