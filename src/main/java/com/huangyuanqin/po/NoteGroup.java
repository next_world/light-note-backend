package com.huangyuanqin.po;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * group
 * @author 
 */
@Data
@Accessors(chain = true)
@TableName(value = "note_group")
public class NoteGroup implements Serializable {
    /**
     * 组id
     */
    @TableId(type = IdType.AUTO,value = "group_id")
    private Integer groupId;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 组名
     */
    @TableField(value = "group_name")
    private String groupName;

    private static final long serialVersionUID = 1L;
}