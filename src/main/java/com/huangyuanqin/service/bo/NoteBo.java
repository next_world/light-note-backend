package com.huangyuanqin.service.bo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanqin.po.Note;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author hyq
 * @date : 2020/5/1  21:28
 */
@Data
@Accessors(chain = true)
public class NoteBo {
    private Page<Note> page;
    private Integer userId;
    private Integer status;
    private Integer groupId;
}
