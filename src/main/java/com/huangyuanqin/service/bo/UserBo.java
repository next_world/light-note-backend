package com.huangyuanqin.service.bo;

import com.huangyuanqin.po.User;
import lombok.Data;

/**
 * @author hyq
 * @date : 2021/1/7  20:29
 */
@Data
public class UserBo {
    private User user;
    private String role;
    private String token;
}
