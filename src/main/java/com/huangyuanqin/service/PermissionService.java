package com.huangyuanqin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanqin.po.Permission;

import java.util.List;

public interface PermissionService extends IService<Permission> {

    /**
     * 获取权限列表
     *
     * @return 权限列表
     */
    List<Permission> getAllPermission();

    /**
     * 根据描述模糊搜索权限
     *
     * @param description 部门描述
     * @return 权限列表
     */
    List<Permission> searchPermission(String description);
}
