package com.huangyuanqin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanqin.controller.vo.NoteVo;
import com.huangyuanqin.po.Note;

/**
 * @author hyq
 */
public interface NoteService extends IService<Note> {
    /**
     * 分页获取note
     * @param currentPage 获取的页
     * @param limit 获取的页的数量
     * @param userId 用户id
     * @param status note状态
     * @param groupId 分组id，空值为查询全部分组
     * @return note的vo
     */
    NoteVo getNote(Integer currentPage, Integer limit, Integer userId, Integer status, Integer groupId);


    /**
     * 更新备忘录
     * @param note Note类
     * @return Boolean
     */
    Boolean updateNote(Note note);

    /**
     * 删除note
     * @param noteId noteId
     * @return 操作结果
     */
    boolean deleteNote(Integer noteId);

    /**
     * 添加note
     * @param note note
     * @return 操作结果
     */
    boolean addNote(Note note);
}
