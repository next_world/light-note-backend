package com.huangyuanqin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanqin.po.NoteGroup;

import java.util.List;

/**
 * @author hyq
 */
public interface NoteGroupService extends IService<NoteGroup> {

    /**
     * 获取用户的所有组
     * @param userId 用户id
     * @return 组列表
     */
    List<NoteGroup> getGroupList(Integer userId);

    /**
     *添加组
     * @param group
     * @return
     */
    boolean addGroup(NoteGroup group);

    /**
     * 删除组
     * @param groupId
     * @return
     */
    boolean deleteGroup(Integer groupId);
}
