package com.huangyuanqin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanqin.po.Role;

/**
 * @author hyq
 */
public interface RoleService extends IService<Role> {

//    /**
//     * 获取角色
//     *
//     * @return 角色列表（包含权限）
//     */
//    List<RolePermissionBo> getRole();
//
//    /**
//     * 删除角色
//     *
//     * @param roleId 角色id
//     * @return 操作结果
//     */
//    boolean deleteRole(Integer roleId);
//
//    /**
//     * 新增角色
//     *
//     * @param role 角色实体类
//     * @return 操作结果
//     */
//    boolean addRole(Role role);
//
//    /**
//     * 更新角色信息
//     *
//     * @param role 角色实体类
//     * @return 操作结果
//     */
//    boolean updateRole(Role role);
//
//    /**
//     * 给角色授权
//     *
//     * @param authorizeBo 包含角色id和权限id列表的Bo
//     * @return 操作结果
//     */
//    boolean authorize(AuthorizeBo authorizeBo);
//
//    /**
//     * 删除某个角色的一个权限
//     *
//     * @param roleId       角色id
//     * @param permissionId 权限id
//     * @return 操作结果
//     */
//    boolean unAuthorize(Integer roleId, Integer permissionId);
//
//    /**
//     * 根据角色名称搜索角色及其权限信息（模糊搜索）
//     *
//     * @param current 页码
//     * @param size 页面大小
//     * @param name 角色名称
//     * @return 分页信息
//     */
//    Page<RoleBo> getRoleByName(Integer current, Integer size, String name);
}
