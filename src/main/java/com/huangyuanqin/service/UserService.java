package com.huangyuanqin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanqin.controller.vo.UpdatePasswordVo;
import com.huangyuanqin.po.User;
import com.huangyuanqin.service.bo.UserBo;


/**
 * @author 12865
 */
public interface UserService extends IService<User> {


    /**
     * 更新用户信息
     * @param user User类
     * @return 修改后的用户
     */
    User updateUserInfo(User user);



    /**
     * 更新密码
     * @param updatePasswordVo updatePassword的vo
     * @return boolean值
     */
    boolean updateUserPassword(UpdatePasswordVo updatePasswordVo);

    /**
     * 注册功能
     * @param user
     * @return
     */
    boolean register(User user);

    /**
     * 登陆
     *
     * @param user 登陆的用户
     * @return 登陆成功的用户
     */
    UserBo login(User user);
}
