package com.huangyuanqin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanqin.po.UserRole;

/**
 * @author pw
 */
public interface UserRoleService extends IService<UserRole> {

    /**
     * 给用户添加角色
     *
     * @param userRole 用户和角色对应关系的实体
     * @return 操作结果
     */
    boolean addUserRole(UserRole userRole);

    /**
     * 删除用户的角色
     *
     * @param id 用户与角色对应关系的id（不是用户的id）
     * @return 操作结果
     */
    boolean deleteUserRole(Long id);
}
