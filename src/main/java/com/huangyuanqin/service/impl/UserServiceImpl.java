package com.huangyuanqin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanqin.auth.dto.SignResult;
import com.huangyuanqin.auth.util.SecurityUtil;
import com.huangyuanqin.common.RoleConstant;
import com.huangyuanqin.controller.vo.UpdatePasswordVo;
import com.huangyuanqin.dao.*;
import com.huangyuanqin.po.*;
import com.huangyuanqin.service.UserService;
import com.huangyuanqin.service.bo.UserBo;
import com.huangyuanqin.util.MD5Util;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author hyq
 * @date : 2020/4/28  2:00
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Resource
    private RolePermissionMapper rolePermissionMapper;

    @Resource
    private PermissionMapper permissionMapper;

    @Resource
    private SecurityUtil securityUtil;

    @Override
    public User updateUserInfo(User user) {
        int i = userMapper.updateById(user);
        if (i > 0) {
            user = userMapper.selectById(user.getUserId());
        }
        return user;
    }

    @Override
    public boolean updateUserPassword(UpdatePasswordVo updatePasswordVo) {
       User user = userMapper.selectById(updatePasswordVo.userId);
       user.setPassword(updatePasswordVo.newPassWord);
       int i = userMapper.updateById(user);
       return i > 0;
    }

    /**
     * 注册功能
      * @param user
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean register(User user) {
        Assert.notNull(user, "用户不能为空");
        Assert.notNull(user.getAccount(), "账号不能为空");
        Assert.notNull(user.getPassword(), "密码不能为空");
        Assert.notNull(user.getUserName(), "用户名不能为空");

        //检索出数据库中与新注册邮箱账号相同的用户
        User user1 = userMapper.getUserByAccount(user.getAccount());
        if (!Objects.isNull(user1)) {
            return false;
        }
        String md5Password = MD5Util.MD5Encode(user.getPassword(), null);
        user.setPassword(md5Password);
        boolean result = userMapper.insert(user) > 0;
        //默认添加用户角色
        if (result) {
            Role role = roleMapper.getRoleByRoleName(RoleConstant.USER_ROLE);
            UserRole userRole = new UserRole();
            userRole.setUserId(user.getUserId());
            userRole.setRoleId(role.getId());
            result = result && userRoleMapper.insert(userRole) > 0;
        }
        return result;
    }

    @Override
    public UserBo login(User user) {
        Assert.notNull(user, "用户不应为空");
        Assert.notNull(user.getAccount(), "账号不应为空");
        Assert.notNull(user.getPassword(), "密码不应为空");

        User user1 = userMapper.getUserByAccount(user.getAccount());
        if (Objects.isNull(user1)) {
            return null;
        }
        String password = MD5Util.MD5Encode(user.getPassword(), null);
        if (password.equals(user1.getPassword())) {
            List<UserRole> userRoleList = userRoleMapper.selectUserRoleByUserId(user1.getUserId());
            UserRole userRole = userRoleList.get(0);
            List<RolePermission> rolePermissionList = rolePermissionMapper.getPermissionIdList(userRole.getRoleId());
            List<Permission> permissionList = rolePermissionList
                    .stream()
                    .map((e) -> permissionMapper.getPermissionById(e.getPermissionId()))
                    .collect(Collectors.toList());
            List<String> permissionStringList = permissionList
                    .stream()
                    .map(Permission::getName)
                    .collect(Collectors.toList());

            //调用权限框架的登陆获取token
            SignResult signResult = securityUtil.login(user1.getUserId().toString(), permissionStringList, user1);

            if (!signResult.isSuccess()) {
                return null;
            }
            Role role = roleMapper.selectById(userRole.getRoleId());
            UserBo userBo = new UserBo();
            userBo.setUser(user1);
            userBo.setRole(role.getName());
            userBo.setToken(signResult.getToken());
            return userBo;
        }
        return null;
    }

}

