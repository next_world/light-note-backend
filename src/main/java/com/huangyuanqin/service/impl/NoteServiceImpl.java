package com.huangyuanqin.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanqin.controller.vo.NoteVo;
import com.huangyuanqin.dao.NoteGroupMapper;
import com.huangyuanqin.dao.NoteMapper;
import com.huangyuanqin.po.NoteGroup;
import com.huangyuanqin.po.Note;
import com.huangyuanqin.service.NoteService;
import com.huangyuanqin.service.bo.NoteBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author hyq
 * @date : 2020/4/28  2:00
 */
@Service
@Slf4j
public class NoteServiceImpl extends ServiceImpl<NoteMapper, Note> implements NoteService {

    @Resource
    private NoteMapper noteMapper;

    @Resource
    private NoteGroupMapper noteGroupMapper;

    @Override
    public NoteVo getNote(Integer currentPage, Integer limit, Integer userId,
                          Integer status, Integer groupId){
        Assert.notNull(currentPage,"当前页不应未空");
        Assert.notNull(limit,"每页数量不应为空");

        Page<Note> page = new Page<>(currentPage,limit);
        NoteBo noteBo = new NoteBo().setPage(page)
                .setUserId(userId)
                .setStatus(status)
                .setGroupId(groupId);
        Page<Note> notePage = null;
        if(Objects.isNull(status)){
            notePage = noteMapper.getAllNote(noteBo);
        } else if(Objects.isNull(groupId)){
            notePage = noteMapper.getNoteByStatus(noteBo);
        }else {
            notePage = noteMapper.getNoteByStatusAndGroup(noteBo);
        }
        NoteVo noteVo = new NoteVo().setNoteList(notePage.getRecords())
                .setPage(notePage.getCurrent())
                .setTotal(notePage.getTotal());
        return noteVo;
    }

    @Override
    public Boolean updateNote(Note note) {
        Assert.notNull(note,"note不应为空");
        Assert.notNull(note.getNoteId(),"noteId不应为空");
        Note oldNote = noteMapper.selectById(note.getNoteId());
        if(oldNote==null){
            return false;
        }
        return noteMapper.updateById(note)>0;
    }

    @Override
    public boolean deleteNote(Integer noteId) {
        Assert.notNull(noteId,"noteId不应为空");
        Note note = noteMapper.selectById(noteId);
        if(note==null){
            return false;
        }
        return noteMapper.deleteById(noteId)>0;
    }

    @Override
    public boolean addNote(Note note) {
        Assert.notNull(note,"note不能为空");
        Assert.notNull(note.getGroupId(),"组id不能为空");
        Assert.notNull(note.getUserId(),"用户id不能为空");
        NoteGroup group = noteGroupMapper.selectById(note.getGroupId());
        if(Objects.isNull(group)){
            return false;
        }
        noteMapper.insert(note);
        return true;
    }
}
