package com.huangyuanqin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanqin.dao.NoteGroupMapper;
import com.huangyuanqin.po.NoteGroup;
import com.huangyuanqin.service.NoteGroupService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hyq
 * @date : 2020/4/28  2:00
 */
@Service
public class NoteGroupServiceImpl extends ServiceImpl<NoteGroupMapper, NoteGroup> implements NoteGroupService {

    @Resource
    private NoteGroupMapper noteGroupMapper;

    @Override
    public List<NoteGroup> getGroupList(Integer userId) {
        Assert.notNull(userId,"用户id不能为空");
        return noteGroupMapper.getGroupByUserId(userId);
    }

    @Override
    public boolean addGroup(NoteGroup group) {
        Assert.notNull(group.getUserId(),"用户id不能为空");
        Assert.notNull(group.getGroupName(),"组名不应为空");
        return noteGroupMapper.insert(group)>0;
    }

    @Override
    public boolean deleteGroup(Integer groupId) {
        Assert.notNull(groupId,"组id不应为空");
        return noteGroupMapper.deleteById(groupId)>0;
    }
}
