package com.huangyuanqin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanqin.dao.RolePermissionMapper;
import com.huangyuanqin.po.RolePermission;
import com.huangyuanqin.service.RolePermissionService;
import org.springframework.stereotype.Service;

@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
