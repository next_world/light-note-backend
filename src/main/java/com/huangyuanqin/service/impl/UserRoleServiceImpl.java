package com.huangyuanqin.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanqin.dao.RoleMapper;
import com.huangyuanqin.dao.UserMapper;
import com.huangyuanqin.dao.UserRoleMapper;
import com.huangyuanqin.po.Role;
import com.huangyuanqin.po.User;
import com.huangyuanqin.po.UserRole;
import com.huangyuanqin.service.UserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;

@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    /**
     * 普通用户角色的名称
     */
    private final static String ROLE_USER = "user";

    @Resource
    private UserMapper userMapper;

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Override
    public boolean addUserRole(UserRole userRole) {
        User user = userMapper.selectById(userRole.getUserId());
        Assert.notNull(user, "没有此用户");

        Role role = roleMapper.selectById(userRole.getRoleId());
        Assert.notNull(role, "没有此角色");

        UserRole ur = userRoleMapper.selectUserRole(userRole.getUserId(), userRole.getRoleId());
        Assert.isNull(ur, "该用户已具有该角色");

        return save(userRole);
    }

    @Override
    public boolean deleteUserRole(Long id) {
        UserRole userRole = getById(id);
        // 判断该id所指向的用户与角色的对应关系是否存在
        Assert.notNull(userRole, "没有此用户与角色关系");

        Role role = roleMapper.selectById(userRole.getRoleId());
        // 判断该id所指向的是否不是用户与普通用户（user）的对应关系，如果是，则抛出异常
        Assert.isTrue(!ROLE_USER.equals(role.getName()), "用户最低角色为普通用户，不能删除");
        return removeById(id);
    }
}
