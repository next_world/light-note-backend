package com.huangyuanqin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanqin.dao.RoleMapper;
import com.huangyuanqin.po.Role;
import com.huangyuanqin.service.RoleService;
import org.springframework.stereotype.Service;

/**
 * @author hyq
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
//
//    @Resource
//    private RoleMapper roleMapper;
//
//    @Resource
//    private RolePermissionMapper rolePermissionMapper;
//
//    @Resource
//    private PermissionMapper permissionMapper;
//
//    @Resource
//    private UserRoleMapper userRoleMapper;
//
//    @Override
//    public List<RolePermissionBo> getRole() {
//        List<RolePermissionBo> rolePermissionBoList = new ArrayList<>();
//        // 获取角色列表
//        List<Role> roleList = roleMapper.getRoleList();
//        for (Role role : roleList) {
//            List<Permission> permissionList = new ArrayList<>();
//            // 根据角色id获取该角色和权限的对应关系列表
//            List<RolePermission> rolePermissionList = rolePermissionMapper.getPermissionIdList(role.getId());
//            for (RolePermission rolePermission : rolePermissionList) {
//                // 根据权限id获取权限
//                Permission permission = permissionMapper.getPermissionById(rolePermission.getPermissionId());
//                permissionList.add(permission);
//            }
//
//            RolePermissionBo rolePermissionBo = new RolePermissionBo();
//            rolePermissionBo.setId(role.getId());
//            rolePermissionBo.setName(role.getName());
//            rolePermissionBo.setDescription(role.getDescription());
//            rolePermissionBo.setPermissionList(permissionList);
//            rolePermissionBoList.add(rolePermissionBo);
//        }
//        return rolePermissionBoList;
//    }
//
//    @Override
//    public boolean deleteRole(Integer roleId) {
//        Role role = getById(roleId);
//        Assert.notNull(role, "没有此角色");
//        Assert.isTrue(DeleteTagConstant.NOT_DELETED.equals(role.getDeleted()), "此角色已被删除");
//
//        List<UserRole> userRoleList = userRoleMapper.selectUserRoleByRoleId(roleId);
//        Assert.isTrue(CollectionUtils.isEmpty(userRoleList), "存在用户与此角色绑定");
//        role.setDeleted(DeleteTagConstant.DELETED);
//        return updateById(role);
//    }
//
//    @Override
//    public boolean addRole(Role role) {
//        Role roleWithSameName = roleMapper.getRoleByRoleName(role.getName());
//        Assert.isTrue(Objects.isNull(roleWithSameName), "已存在该名称的角色");
//        return save(role);
//    }
//
//    @Override
//    public boolean updateRole(Role role) {
//        Role oldRole = getById(role.getId());
//        // 判断是否存在该角色数据 且 该角色是否未被删除
//        Assert.isTrue(Objects.nonNull(oldRole) && DeleteTagConstant.NOT_DELETED.equals(oldRole.getDeleted()), "该角色不存在");
//        Role roleWithSameName = roleMapper.getRoleByRoleName(role.getName());
//        // 判断是否存在相同角色名称的 其他 角色
//        Assert.isTrue(Objects.isNull(roleWithSameName) || oldRole.getId().equals(roleWithSameName.getId()), "已存在该名称的角色");
//        return updateById(role);
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public boolean authorize(AuthorizeBo authorizeBo) {
//        Role role = roleMapper.selectById(authorizeBo.getRoleId());
//        Assert.notNull(role, "没有要授权的角色");
//        List<Permission> permissionList = permissionMapper.selectBatchIds(authorizeBo.getPermissionIds());
//        //判断权限数量和权限id数量是否相等已判断是否都是有效的权限
//        Assert.isTrue(permissionList.size() == authorizeBo.getPermissionIds().size(), "没有要授权的权限");
//        //权限查重
//        List<RolePermission> permissionIdList = rolePermissionMapper.getPermissionIdList(role.getId());
//        Set<Integer> rolePermissionIds = permissionIdList.stream().map(RolePermission::getPermissionId).collect(Collectors.toSet());
//        rolePermissionIds.retainAll(authorizeBo.getPermissionIds());
//        Assert.isTrue(rolePermissionIds.isEmpty(), "该角色已有授权列表下的权限");
//        authorizeBo.getPermissionIds().stream().map((permissionId) -> {
//            RolePermission rolePermission = new RolePermission();
//            rolePermission.setRoleId(authorizeBo.getRoleId());
//            rolePermission.setPermissionId(permissionId);
//            return rolePermission;
//        }).forEach(rolePermissionMapper::insert);
//        return true;
//    }
//
//    @Override
//    public boolean unAuthorize(Integer roleId, Integer permissionId) {
//        Role role = roleMapper.selectById(roleId);
//        Assert.notNull(role, "没有该角色");
//        Permission permission = permissionMapper.selectById(permissionId);
//        Assert.notNull(permission, "没有该权限");
//        RolePermission rolePermission = rolePermissionMapper
//                .selectRolePermissionByRoleIdAndPermissionId(roleId, permissionId);
//
//        Assert.notNull(rolePermission, "该角色没有这样的权限");
//        return rolePermissionMapper.deleteById(rolePermission.getId()) > 0;
//    }
//
//    @Override
//    public Page<RoleBo> getRoleByName(Integer current, Integer size, String name) {
//        Page<Role> rolePage =  roleMapper.getRoleLikeName(current, size, name);
//        List<RoleBo> roleBoList = new LinkedList<>();
//
//        if (!CollectionUtils.isEmpty(rolePage.getRecords())) {
//
//            List<Integer> roleIdList = rolePage.getRecords().stream().map(Role::getId).collect(Collectors.toList());
//            List<RolePermission> list = rolePermissionMapper.getRolePermissionListByRoleIdList(roleIdList);
//            // 对不同角色的权限进行分组
//            Map<Integer, List<RolePermission>> group = list.stream().collect(Collectors.groupingBy(RolePermission::getRoleId));
//
//            for (Role role : rolePage.getRecords()) {
//                // 根据角色id获取该角色和权限的对应关系列表
//                List<RolePermission> rolePermissionList = group.get(role.getId());
//                List<Permission> permissionList = null;
//                if (!CollectionUtils.isEmpty(rolePermissionList)) {
//                    List<Integer> idList = rolePermissionList.stream().map(RolePermission::getPermissionId).collect(Collectors.toList());
//                    permissionList = permissionMapper.selectBatchIds(idList);
//                }
//                RoleBo roleBo = RoleBo.convert(role);
//                roleBo.setPermissionList(permissionList);
//                roleBoList.add(roleBo);
//            }
//        }
//
//        Page<RoleBo> roleBoPage = new Page<>();
//        BeanUtils.copyProperties(rolePage, roleBoPage);
//        roleBoPage.setRecords(roleBoList);
//        return roleBoPage;
//    }
}
