package com.huangyuanqin.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanqin.dao.PermissionMapper;
import com.huangyuanqin.po.Permission;
import com.huangyuanqin.service.PermissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Resource
    private PermissionMapper permissionMapper;

    @Override
    public List<Permission> getAllPermission() {
        return permissionMapper.getAllPermission();
    }

    @Override
    public List<Permission> searchPermission(String description) {
        return permissionMapper.searchPermission(description);
    }
}
