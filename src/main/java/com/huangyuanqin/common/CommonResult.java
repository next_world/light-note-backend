package com.huangyuanqin.common;

import lombok.Data;
import lombok.experimental.Accessors;

import static com.huangyuanqin.common.CommonConstant.*;

/**
 * @author hyq
 * @date : 2020/4/30  17:04
 */
@Data
@Accessors(chain = true)
public class CommonResult<T> {
    private Integer code;
    private String message;
    private boolean isSuccess;
    private T data;

    public CommonResult<T> operateSuccess() {
        return setSuccess(true).setMessage(OPERATE_SUCCESS_MESSAGE).setCode(SUCCESS_CODE);
    }

    public CommonResult<T> operateSuccess(T t) {
        return operateSuccess().setData(t);
    }

    public CommonResult<T> operateFail() {
        return setSuccess(false).setMessage(OPERATE_FAIL_MESSAGE).setCode(FAIL_CODE);
    }

    public CommonResult<T> operateFail(T t) {
        return operateFail().setData(t);
    }

    public CommonResult<T> operateNoPower() {
        return setSuccess(false).setMessage(OPERATE_NO_POWER_MESSAGE).setCode(FORBIDDEN_CODE);
    }

    public CommonResult<T> operateNoPower(T t) {
        return operateFail().setData(t);
    }

    public <P> CommonResult<P> autoResult(boolean isSuccess) {
        if (isSuccess) {
            return new CommonResult<P>().operateSuccess();
        } else {
            return new CommonResult<P>().operateFail();
        }
    }

    public CommonResult<T> autoResult(boolean isSuccess, T data) {
        if (isSuccess) {
            return new CommonResult<T>().operateSuccess(data);
        } else {
            return new CommonResult<T>().operateFail(data);
        }
    }

}
