package com.huangyuanqin.common;

/**
 * @author hyq
 * @date : 2020/4/30  17:07
 */
public class CommonConstant {
    public static final String OPERATE_SUCCESS_MESSAGE = "success";
    public static final String OPERATE_ALREADY_DONE = "you have done before";
    public static final String OPERATE_FAIL_MESSAGE = "fail";
    public static final String OPERATE_NO_POWER_MESSAGE = "no power";

    public static final int SUCCESS_CODE = 200;
    public static final int FAIL_CODE = 400;
    public static final int FORBIDDEN_CODE = 403;
}
