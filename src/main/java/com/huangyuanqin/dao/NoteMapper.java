package com.huangyuanqin.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanqin.po.Note;
import com.huangyuanqin.service.bo.NoteBo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hyq
 */
@Mapper
public interface NoteMapper extends BaseMapper<Note> {

    /**
     * 根据note状态和group获取note
     * @param noteBo {@link NoteBo}
     * @return note的page
     */
    default Page<Note> getNoteByStatusAndGroup(NoteBo noteBo){
        QueryWrapper<Note> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Note::getUserId,noteBo.getUserId())
                .eq(Note::getStatus,noteBo.getStatus())
                .eq(Note::getGroupId,noteBo.getGroupId());
        return selectPage(noteBo.getPage(),queryWrapper);
    }

    /**
     * 根据状态获取note
     * @param noteBo {@link NoteBo}
     * @return note的page
     */
    default Page<Note> getNoteByStatus(NoteBo noteBo){
        QueryWrapper<Note> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Note::getUserId,noteBo.getUserId())
                .eq(Note::getStatus,noteBo.getStatus());
        return selectPage(noteBo.getPage(),queryWrapper);
    }

    /**
     * 获取所有note
     * @param noteBo {@link NoteBo}
     * @return note的page
     */
    default Page<Note> getAllNote(NoteBo noteBo){
        QueryWrapper<Note> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Note::getUserId,noteBo.getUserId());
        return selectPage(noteBo.getPage(),queryWrapper);
    }
}