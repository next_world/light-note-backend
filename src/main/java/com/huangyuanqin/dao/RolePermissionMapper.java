package com.huangyuanqin.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanqin.po.RolePermission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author pw
 */
@Mapper
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

    /**
     * 根据角色id查询权限id列表
     *
     * @param roleId 角色id
     * @return 权限id列表
     */
    default List<RolePermission> getPermissionIdList(Integer roleId) {
        QueryWrapper<RolePermission> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .select(RolePermission::getPermissionId)
                .eq(RolePermission::getRoleId, roleId);
        return selectList(queryWrapper);
    }

    /**
     * 通过角色id列表批量查询RolePermission列表
     *
     * @param idList 角色id列表
     * @return 角色权限关系列表
     */
    default List<RolePermission> getRolePermissionListByRoleIdList(List<Integer> idList) {
        QueryWrapper<RolePermission> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .select(RolePermission::getId, RolePermission::getPermissionId, RolePermission::getRoleId)
                .in(RolePermission::getRoleId, idList);
        return selectList(queryWrapper);
    }

    /**
     * 根据角色id和权限id查询角色权限关系
     *
     * @param roleId       角色id
     * @param permissionId 权限id
     * @return 查询到的角色权限关系
     */
    default RolePermission selectRolePermissionByRoleIdAndPermissionId(Integer roleId, Integer permissionId) {
        QueryWrapper<RolePermission> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().select(RolePermission::getId, RolePermission::getPermissionId, RolePermission::getRoleId)
                .eq(RolePermission::getRoleId, roleId).eq(RolePermission::getPermissionId, permissionId);
        return selectOne(queryWrapper);
    }
}