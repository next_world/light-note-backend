package com.huangyuanqin.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanqin.po.UserRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 根据用户id获取用户角色关系
     *
     * @param userId 用户id
     * @return 用户角色关系
     */
    default List<UserRole> selectUserRoleByUserId(Integer userId) {
        QueryWrapper<UserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .select(UserRole::getId, UserRole::getUserId, UserRole::getRoleId)
                .eq(UserRole::getUserId, userId);
        return selectList(queryWrapper);
    }

    /**
     * 根据用户id和角色id查询该用户与该角色的对应关系
     *
     * @param userId 用户的id
     * @param roleId 角色的id
     * @return 用户与角色的对应关系
     */
    default UserRole selectUserRole(Integer userId, Integer roleId) {
        QueryWrapper<UserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(UserRole::getUserId, userId)
                .eq(UserRole::getRoleId, roleId);
        return selectOne(queryWrapper);
    }

    /**
     * 根据角色id查询用户与角色的对应关系列表
     *
     * @param roleId 角色id
     * @return 对应关系列表
     */
    default List<UserRole> selectUserRoleByRoleId(Integer roleId) {
        QueryWrapper<UserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .select(UserRole::getId, UserRole::getUserId, UserRole::getRoleId)
                .eq(UserRole::getRoleId, roleId);
        return selectList(queryWrapper);
    }
}