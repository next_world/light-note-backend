package com.huangyuanqin.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanqin.po.Permission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author pw
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {

    /**
     * 根据权限id查询权限
     *
     * @param permissionId 权限id
     * @return 权限实体
     */
    default Permission getPermissionById(Integer permissionId) {
        QueryWrapper<Permission> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .select(Permission::getId, Permission::getName)
                .eq(Permission::getId, permissionId);
        return selectOne(queryWrapper);
    }

    /**
     * 查询所有权限
     *
     * @return 权限列表
     */
    default List<Permission> getAllPermission() {
        QueryWrapper<Permission> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .select(Permission::getId, Permission::getName, Permission::getDescription);
        return selectList(queryWrapper);
    }

    /**
     * 根据描述模糊查询权限
     *
     * @param description 权限描述
     * @return 权限列表
     */
    default List<Permission> searchPermission(String description) {
        QueryWrapper<Permission> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .select(Permission::getId, Permission::getName, Permission::getDescription)
                .like(Permission::getDescription, description);
        return selectList(queryWrapper);
    }
}