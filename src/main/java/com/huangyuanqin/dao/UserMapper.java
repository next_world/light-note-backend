package com.huangyuanqin.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanqin.po.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hyq
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据账号查找用户
     * @param account 账号
     * @return 用户
     */
    default User getUserByAccount(String account){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(User::getAccount,account);
        return selectOne(queryWrapper);
    }


}