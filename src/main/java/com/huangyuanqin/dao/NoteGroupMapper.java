package com.huangyuanqin.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanqin.po.NoteGroup;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author hyq
 */
@Mapper
public interface NoteGroupMapper extends BaseMapper<NoteGroup> {

    /**
     * 根据用户id获取组别
     * @param userId 用户id
     * @return 组的列表
     */
    default List<NoteGroup> getGroupByUserId(Integer userId){
        QueryWrapper<NoteGroup> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(NoteGroup::getUserId,userId);
        return selectList(queryWrapper);
    }
}