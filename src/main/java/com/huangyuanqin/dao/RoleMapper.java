package com.huangyuanqin.dao;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanqin.po.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Optional;

/**
 * @author pw
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 查询角色列表
     *
     * @return 角色列表
     */
    default List<Role> getRoleList() {
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .select(Role::getId, Role::getName, Role::getDescription);
        return selectList(queryWrapper);
    }

    /**
     * 根据角色名查询未被删除的角色
     *
     * @param roleName 角色名
     * @return 角色
     */
    default Role getRoleByRoleName(String roleName) {
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().select(Role::getId, Role::getName)
                .eq(Role::getName, roleName);
        return selectOne(queryWrapper);
    }

    /**
     * 根据角色名称模糊搜索角色
     *
     * @param current 当前页码
     * @param size    页面大小
     * @param name    角色名称
     * @return 分页信息
     */
    default Page<Role> getRoleLikeName(Integer current, Integer size, String name) {
        // name为null则默认查询全部
        name = Optional.ofNullable(name).orElse("");
        Page<Role> page = new Page<>();
        page.setCurrent(current).setSize(size);
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .select(Role::getId, Role::getName, Role::getDescription)
                .like(Role::getName, name);
        return selectPage(page, queryWrapper);
    }
}